import pytest
from django.contrib.auth.models import User
from django.test import Client
from django.urls import reverse

from grouptask.models import Task
from grouptask.testing import login


@pytest.mark.django_db
def test_login(client: Client, django_user_model: User):
    django_user_model.objects.create_user(
        'pirmas',
        'test@example.com',
        'first_password',
    )

    # Client visits Signin page
    resp = client.get('/accounts/signup/')
    assert resp.status_code == 200
    assert 'form' in resp.context

    # Existed Client visits home page
    resp = client.post('/accounts/login/', {
    'username': 'pirmas',
    'password': 'first_password',
    })
    assert resp.status_code == 302, resp.context['form'].errors.as_text()
    assert resp['location'] == '/login/'

    # Client gets redirect to Login page
    resp = client.get('/accounts/login/')
    assert resp.status_code == 200
    assert 'form' in resp.context



def test_create_task(client: Client, django_user_model: User):
    user = django_user_model.objects.create_user(
        'pirmas',
        'test@example.com',
        'first_password',
    )

    # Visit task list page
    resp = client.get(reverse('list_tasks'))
    assert resp.status_code == 302

    # Create a new task
    # resp = Task.objects.get('/tasks/add/', {
    #     'title': 'ABC',
    #     'description': 'doing',
    #     # 'user': user.pk,
    # })
    # assert resp.status_code == 302, resp.context['form'].errors.as_text()
    # task = Task.objects.get(title='ABC', description= 'doing')
    # assert resp['location'] == 'list_tasks'
