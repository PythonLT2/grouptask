from django.contrib import admin

from grouptask.models import Project, Task, TodoList

admin.site.register(Project)
admin.site.register(Task)
admin.site.register(TodoList)
