from django.contrib import admin
from django.urls import path, include

from . import views

urlpatterns = [
    path('', views.IndexView.as_view(), name='home'),

    path('accounts/', include('grouptask.accounts.urls'), name='accounts'),
    path('accounts/', include('django.contrib.auth.urls')),
    path('login/', views.LoginConfView.as_view(), name='login_conf'),
    path('logout/', views.LogoutConfView.as_view(), name='logout_conf'),

    path('projects/', views.ProjectListView.as_view(), name='projects'),
    path('projects/<int:pk>/', views.ProjectDetailView.as_view(), name='project_detail'),
    path('projects/create/', views.ProjectCreateView.as_view(), name='project_create'),
    path('projects/<int:pk>/update/', views.ProjectUpdateView.as_view(), name='project_update'),
    path('projects/<int:pk>/delete/', views.ProjectDeleteView.as_view(), name='project_delete'),

    path('tasks/list/', views.TasksListView.as_view(), name='list_tasks'),
    path('tasks/add/', views.create_task, name='create_task'),
    path('tasks/task_update/<str:pk>/', views.update_task, name='update'),
    path('tasks/task_remove/<str:pk>/', views.remove_task, name='remove'),
    path('tasks/todo/', views.TaskTodoListView.as_view(), name='list_task_todos'),
    path('tasks/todo_update/<str:pk>/', views.update_todo, name='update_todo'),
    path('tasks/todo/add/', views.create_task_todo, name='create_task_todo'),
    path('tasks/todo/remove/<str:pk>/', views.remove_todo_task, name='remove_todo'),

    path('admin/', admin.site.urls),
]
