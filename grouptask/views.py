from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.http import HttpRequest
from django.urls import reverse_lazy
from django.views.generic import TemplateView, ListView, DetailView, CreateView, UpdateView, DeleteView

from grouptask.forms import TaskFormCreator, TodoListFormCreator
from grouptask.models import Task, TodoList
from . import models


class IndexView(TemplateView):
    template_name = 'pages/index.html'


class LoginConfView(TemplateView):
    template_name = 'accounts/login_conf.html'


class LogoutConfView(TemplateView):
    template_name = 'accounts/logout_conf.html'


class ProjectListView(LoginRequiredMixin, ListView):
    context_object_name = 'projects'
    model = models.Project
    paginate_by = 5


class ProjectDetailView(LoginRequiredMixin, DetailView):
    context_object_name = 'project_detail'
    model = models.Project
    template_name = 'grouptask/project_detail.html'


class ProjectCreateView(LoginRequiredMixin, CreateView):
    fields = ('project_name', )
    model = models.Project
    success_url = reverse_lazy('projects')


class ProjectUpdateView(LoginRequiredMixin, UpdateView):
    fields = ('project_name', )
    model = models.Project


class ProjectDeleteView(LoginRequiredMixin, DeleteView):
    model = models.Project
    success_url = reverse_lazy('projects')


class TasksListView(LoginRequiredMixin, ListView):
    template_name = 'pages/tasks/tasks_list.html'
    model = Task
    paginate_by = 3


@login_required
def create_task(request: HttpRequest):
    form = TaskFormCreator()
    if request.method == 'POST':
        form = TaskFormCreator(request.POST)
        if form.is_valid():
            task = form.save()
            return redirect('/tasks/list', task.pk)
    return render(request, 'pages/tasks/tasks_form.html', {
        'list': TasksListView, 'form': form,
    })


@login_required
def update_task(request, pk):
    task = Task.objects.get(id=pk)
    form = TaskFormCreator(instance=task)
    if request.method == 'POST':
        form = TaskFormCreator(request.POST, instance=task)
        if form.is_valid():
            form.save()
            return redirect('/tasks/list')
    context = {'form': form}
    return render(request, 'pages/tasks/task_update.html', context)


@login_required
def remove_task(request, pk):
    item = Task.objects.get(id=pk)
    if request.method == 'POST':
        item.delete()
        return redirect('/tasks/list')
    context = {'item': item}
    return render(request, 'pages/tasks/task_remove.html', context)


class TaskTodoListView(LoginRequiredMixin, ListView):
    template_name = 'pages/todo/todo_list.html'
    model = TodoList
    paginate_by = 3


@login_required
def create_task_todo(request: HttpRequest):
    form = TodoListFormCreator()
    if request.method == 'POST':
        form = TodoListFormCreator(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/tasks/todo/')
    context = {'list': TaskTodoListView, 'form': form}
    return render(request, 'pages/todo/todo_form.html', context)


@login_required
def update_todo(request, pk):
    todo = TodoList.objects.get(id=pk)
    form = TodoListFormCreator(instance=todo)
    if request.method == 'POST':
        form = TodoListFormCreator(request.POST, instance=todo)
        if form.is_valid():
            form.save()
            return redirect('/tasks/todo')
    context = {'form': form}
    return render(request, 'pages/todo/todo_update.html', context)


@login_required
def remove_todo_task(request, pk):
    item = TodoList.objects.get(id=pk)
    if request.method == 'POST':
        item.delete()
        return redirect('/tasks/todo')
    context = {'item': item}
    return render(request, 'pages/todo/todo_remove.html', context)

