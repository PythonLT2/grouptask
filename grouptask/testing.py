from django.contrib.auth.models import User
from django.test.client import Client


def login(client: Client) -> User:
    user = User.objects.create(
        username='first',
        first_name='Jone',
        last_name='Bath',
        email='testeris@example.com',
    )
    user.set_password('123456'),
    client.login(username='first', password='123456')
    return user
