from django import forms
from grouptask.models import Task, TodoList


class TaskFormCreator(forms.ModelForm):
    class Meta:
        model = Task
        fields = [
            'project_name',
            'title',
            'description',
            'finish',
        ]


class TodoListFormCreator(forms.ModelForm):
    class Meta:
        model = TodoList
        fields = [
            'task',
            'todo_title',
            'finish',
            'done'
        ]
