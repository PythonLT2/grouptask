from django.db import models
from django.urls import reverse


class Project(models.Model):
    project_name = models.CharField(max_length=256, blank=False)

    def __str__(self):
        return self.project_name

    def get_absolute_url(self):
        return reverse('project_detail', kwargs={'pk': self.pk})


class Task(models.Model):
    user = models.CharField(max_length=200)
    project_name = models.ForeignKey(Project, related_name='project_creator', default=True, on_delete=models.CASCADE)
    title = models.CharField(max_length=120)
    description = models.TextField(blank=True)
    create = models.DateTimeField(auto_now_add=True)
    finish = models.DateField(null=True, blank=True)

    def __str__(self):
        return self.title


class TodoList(models.Model):
    task = models.ForeignKey(Task, default=True, on_delete=models.CASCADE)
    todo_title = models.TextField(null=True, blank=True)
    create = models.DateTimeField(auto_now_add=True)
    finish = models.DateField(null=True, blank=True)
    done = models.BooleanField(default=False)

    def __str__(self):
        return self.todo_title
