from django.contrib import auth
from django.contrib.auth.models import User


class User(User, auth.models.PermissionsMixin):

    def __str__(self):
        return '@{}'.format(self.username)
